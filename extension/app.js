(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var MessageTools = require('./messageTools');
var Message = React.createClass({displayName: "Message",
  render: function() {
    return (
      React.createElement("div", {className: "list-group-item"}, 
        React.createElement("div", {className: "list-group-item-heading"}, 
          React.createElement("h4", null, 
            this.props.author
          )
        ), 
        React.createElement("p", {className: "list-group-item-text"}, 
          this.props.body
        )
      )
    );
  }
});
module.exports=Message;

},{"./messageTools":4}],2:[function(require,module,exports){
var MessageForm = React.createClass({displayName: "MessageForm",
  render: function() {
    var _visClass = this.props.visible ? 'visible' : 'hidden'
    return (
      React.createElement("div", {className: _visClass}, 
        React.createElement("form", {id: "newMessage"}, 
          React.createElement("div", {className: "form-group"}, 

            React.createElement("div", {className: "input-group input-group-sm"}, 
              React.createElement("span", {className: "input-group-addon", id: "basic-addon1"}, "@"), 
              React.createElement("input", {type: "text", className: "form-control", placeholder: "Username", "aria-describedby": "basic-addon1"})
            )

          ), 
        
          React.createElement("div", {className: "form-group"}, 
            React.createElement("div", {className: "messageBody", contentEditable: "true"}
            )
          ), 

          React.createElement("button", {type: "submit", className: "btn btn-sm btn-primary"}, "Send")

        )
      )
    );
  }
});

module.exports = MessageForm;

},{}],3:[function(require,module,exports){
var Message = require('./message.js');

var MessageList = React.createClass({displayName: "MessageList",
  getInitialState: function() {
    return { messages : [] }
  },
  componentDidMount: function() {
    var that = this;
    chrome.runtime.sendMessage({message: 'pollMessages'});
    chrome.storage.local.get(function(items){
      console.log('message-init : ' + items);
      that.setState({messages : items.messages || []});
    });
    chrome.storage.onChanged.addListener(function(changes, namespace) {
      if(changes.messages) {
        if(changes.messages.oldValue != changes.messages.newValue) {
          console.log('message-update : ' + changes.messages.newValue);
          that.setState({messages: changes.messages.newValue});
        }
      }
    });
  },
  render: function() {
    var messageNodes = this.state.messages.map(function (message) {
      return (
        React.createElement(Message, {key: message.id, author: message.sender_screen_name, 
                 body: message.text})
      );
    });
    return (
      React.createElement("div", {className: "list-group"}, 
        messageNodes
      )
    );
  }
});

module.exports = MessageList;

},{"./message.js":1}],4:[function(require,module,exports){
var MessageTools = React.createClass({displayName: "MessageTools",
  render: function() {
    return (
      React.createElement("ul", {className: "tools"}, 
        React.createElement("li", null, 
          React.createElement("button", {type: "button", className: "btn btn-sm pull-right", onClick: this.composeClick, "aria-label": "Compose"}, 
            React.createElement("span", {className: "glyphicon glyphicon-envelope", "aria-hidden": "true"})
          )
        ), 
        React.createElement("li", null, 
          React.createElement("button", {type: "button", className: "btn btn-sm pull-right", onClick: this.composeClick, "aria-label": "Compose"}, 
            React.createElement("span", {className: "glyphicon glyphicon-remove", "aria-hidden": "true"})
          )
        )
      )
    );
  }
});

module.exports=MessageTools;

},{}],5:[function(require,module,exports){

var SignOn = require('./signOn.js');
var MessageList = require('./messageList.js');
var ToolBar = require('./toolBar.js');

var Popup = React.createClass({displayName: "Popup",
    getInitialState: function() {
      return { authenticated: false }
    },
    componentDidMount: function() {
      var that = this;
      chrome.runtime.sendMessage({message: 'isAuthenticated'}, function(state) {
        console.log('auth-state : ' + state);
        that.setState({ authenticated: state });
      });
    },
    render: function() {
        if(this.state.authenticated) {
          return (
            React.createElement("div", null, 
              React.createElement(ToolBar, null), 
              React.createElement(MessageList, null)
            )
          );
        } else {
          return (
            React.createElement("div", {className: "row"}, 
              React.createElement("div", {className: "col-sm-6 col-md-4"}, 
                React.createElement("div", {className: "thumbnail"}, 
                  React.createElement("img", {src: chrome.extension.getURL('css.jpg'), alt: "stuff"}), 
                  React.createElement("div", {className: "caption"}, 
                    React.createElement("h3", null, "dmBox ", React.createElement("small", null, "twitter messaging")), 
                    React.createElement("p", null, 
                      React.createElement(SignOn, null)
                    )
                  )
                )
              )
            )
          );
        }
        
    }
});

module.exports=Popup;

},{"./messageList.js":3,"./signOn.js":6,"./toolBar.js":7}],6:[function(require,module,exports){

var SignOn = React.createClass({displayName: "SignOn",
    render: function() {
      var _extCb = chrome.extension.getURL('authComplete.html');
      var _authUrl = 'http://dmbox.herokuapp.com/auth/twitter?extcb=' + _extCb;
      return (
        React.createElement("div", null, 
          React.createElement("a", {href: _authUrl, target: "_blank"}, 
            React.createElement("img", {src: "https://g.twimg.com/dev/sites/default/files/images_documentation/sign-in-with-twitter-gray.png", alt: "Sign in with Twitter", title: "Sign in with Twitter"})
          )
        )
      );
    }
});

module.exports=SignOn;

},{}],7:[function(require,module,exports){
var MessageForm = require('./messageForm.js');

var ToolBar = React.createClass({displayName: "ToolBar",
  getInitialState: function() {
    return { composing: false };
  },
  composeClick: function(event) {
    this.setState({composing: !this.state.composing});
  },
  render: function() {
    return (
      React.createElement("div", {className: "toolBar"}, 
        React.createElement("ul", {className: "tools"}, 
          React.createElement("li", null, 
            React.createElement("input", {type: "text", className: "form-control input-sm", placeholder: "Search"})
          ), 
          React.createElement("li", null, 
            React.createElement("button", {type: "button", className: "btn btn-primary pull-right", onClick: this.composeClick, "aria-label": "Compose"}, 
              React.createElement("span", {className: "glyphicon glyphicon-pencil", "aria-hidden": "true"})
            )
          )
        ), 
        React.createElement(MessageForm, {visible: this.state.composing})
      )
    );
  }
});

module.exports=ToolBar;

},{"./messageForm.js":2}],8:[function(require,module,exports){
var Popup = require('./ui/popup.js'); 

React.render(React.createElement(Popup, null), document.getElementById('container'));

},{"./ui/popup.js":5}]},{},[8]);
