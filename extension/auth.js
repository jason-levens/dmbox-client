var anchorString = window.location.hash.substring(1);
var anchorParams = anchorString.split("&");

if (anchorParams.length > 1) {
    chrome.runtime.sendMessage({message: 'scrapeAuthParams', data: anchorParams});
} else {
    var urlParamString = window.location.search.substring(1);
    var urlParams = urlParamString.split("&");
    if (urlParams.length > 1) {
        chrome.runtime.sendMessage({message: 'scrapeAuthParams', data: urlParams});
    }
}