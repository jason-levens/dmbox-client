#!/bin/bash
cp ./node_modules/react/dist/react.js ./extension/lib/js
cp ./node_modules/react/dist/JSXTransformer.js ./extension/lib/js
cp ./node_modules/bootstrap/dist/css/bootstrap.min.css ./extension/lib/css
cp ./node_modules/bootstrap/dist/fonts/* ./extension/lib/fonts
cp ./src/html/* ./extension/
cp ./src/css/* ./extension/
cp ./src/assets/* ./extension/
cp ./src/js/auth.js ./extension/

browserify src/js/app.js -t reactify -o extension/app.js
browserify src/js/background.js -o extension/background.js