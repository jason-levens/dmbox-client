var MessageTools = require('./messageTools');
var Message = React.createClass({
  render: function() {
    return (
      <div className="list-group-item" >
        <div className="list-group-item-heading">
          <h4>
            {this.props.author}
          </h4>  
        </div>
        <p className="list-group-item-text">
          {this.props.body}
        </p>
      </div>
    );
  }
});
module.exports=Message;