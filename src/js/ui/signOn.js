
var SignOn = React.createClass({
    render: function() {
      var _extCb = chrome.extension.getURL('authComplete.html');
      var _authUrl = 'http://dmbox.herokuapp.com/auth/twitter?extcb=' + _extCb;
      return (
        <div>
          <a href={_authUrl} target="_blank" >
            <img src="https://g.twimg.com/dev/sites/default/files/images_documentation/sign-in-with-twitter-gray.png" alt="Sign in with Twitter" title="Sign in with Twitter" />
          </a>
        </div>
      );
    }
});

module.exports=SignOn;