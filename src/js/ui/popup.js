
var SignOn = require('./signOn.js');
var MessageList = require('./messageList.js');
var ToolBar = require('./toolBar.js');

var Popup = React.createClass({
    getInitialState: function() {
      return { authenticated: false }
    },
    componentDidMount: function() {
      var that = this;
      chrome.runtime.sendMessage({message: 'isAuthenticated'}, function(state) {
        console.log('auth-state : ' + state);
        that.setState({ authenticated: state });
      });
    },
    render: function() {
        if(this.state.authenticated) {
          return (
            <div>
              < ToolBar />
              < MessageList />
            </div>
          );
        } else {
          return (
            <div className="row">
              <div className="col-sm-6 col-md-4">
                <div className="thumbnail">
                  <img src={chrome.extension.getURL('css.jpg')} alt="stuff" />
                  <div className="caption">
                    <h3>dmBox <small>twitter messaging</small></h3>
                    <p>
                      < SignOn />
                    </p>
                  </div>
                </div>
              </div>
            </div>
          );
        }
        
    }
});

module.exports=Popup;