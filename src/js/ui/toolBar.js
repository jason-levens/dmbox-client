var MessageForm = require('./messageForm.js');

var ToolBar = React.createClass({
  getInitialState: function() {
    return { composing: false };
  },
  composeClick: function(event) {
    this.setState({composing: !this.state.composing});
  },
  render: function() {
    return (
      <div className="toolBar">
        <ul className="tools">
          <li>
            <input type="text" className="form-control input-sm" placeholder="Search" />
          </li>
          <li>
            <button type="button" className="btn btn-primary pull-right" onClick={this.composeClick} aria-label="Compose">
              <span className="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </button>
          </li>
        </ul>
        < MessageForm visible={this.state.composing}/>
      </div>
    );
  }
});

module.exports=ToolBar;