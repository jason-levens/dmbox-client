var MessageTools = React.createClass({
  render: function() {
    return (
      <ul className="tools">
        <li>
          <button type="button" className="btn btn-sm pull-right" onClick={this.composeClick} aria-label="Compose">
            <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>
          </button>
        </li>
        <li>
          <button type="button" className="btn btn-sm pull-right" onClick={this.composeClick} aria-label="Compose">
            <span className="glyphicon glyphicon-remove" aria-hidden="true"></span>
          </button>
        </li>
      </ul>
    );
  }
});

module.exports=MessageTools;