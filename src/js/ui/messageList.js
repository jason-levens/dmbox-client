var Message = require('./message.js');

var MessageList = React.createClass({
  getInitialState: function() {
    return { messages : [] }
  },
  componentDidMount: function() {
    var that = this;
    chrome.runtime.sendMessage({message: 'pollMessages'});
    chrome.storage.local.get(function(items){
      console.log('message-init : ' + items);
      that.setState({messages : items.messages || []});
    });
    chrome.storage.onChanged.addListener(function(changes, namespace) {
      if(changes.messages) {
        if(changes.messages.oldValue != changes.messages.newValue) {
          console.log('message-update : ' + changes.messages.newValue);
          that.setState({messages: changes.messages.newValue});
        }
      }
    });
  },
  render: function() {
    var messageNodes = this.state.messages.map(function (message) {
      return (
        <Message key={message.id} author={message.sender_screen_name} 
                 body={message.text} />
      );
    });
    return (
      <div className="list-group">
        {messageNodes}
      </div>
    );
  }
});

module.exports = MessageList;