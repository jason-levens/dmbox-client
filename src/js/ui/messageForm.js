var MessageForm = React.createClass({
  render: function() {
    var _visClass = this.props.visible ? 'visible' : 'hidden'
    return (
      <div className={_visClass}>
        <form id="newMessage">
          <div className="form-group">

            <div className="input-group input-group-sm">
              <span className="input-group-addon" id="basic-addon1">@</span>
              <input type="text" className="form-control" placeholder="Username" aria-describedby="basic-addon1" />
            </div>

          </div>
        
          <div className="form-group">
            <div className="messageBody" contentEditable="true">
            </div>
          </div>

          <button type="submit" className="btn btn-sm btn-primary">Send</button>

        </form>
      </div>
    );
  }
});

module.exports = MessageForm;