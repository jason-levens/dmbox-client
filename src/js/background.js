
var _polling = false;

function getMessages() {
  var oReq = new XMLHttpRequest();
  chrome.storage.local.get(function(items) {
    var _url = 'http://dmbox.herokuapp.com/feed?at=' + items.at + '&ats=' + items.ats;
    oReq.onload = function(response) {
      var _messages = JSON.parse(response.currentTarget.response);
      chrome.storage.local.set({messages: _messages});
      chrome.browserAction.setBadgeText({text: _messages.length.toString()});
    }
    oReq.open("get", _url, true);
    oReq.send();
  });
}

function scrapeAuthParams(params) {
  for (var i = 0; i < params.length; i++) {
    var pair = params[i].split("=");
    switch (pair[0]) {
      case "at":
      _at = decodeURIComponent(pair[1]);
      break;
      case "ats":
      _ats = decodeURIComponent(pair[1]);
      break;
    }
  }
  chrome.storage.local.set({at: _at, ats: _ats});
}

function isAuthenticated(callback) {
  var _authenticated = false;
  chrome.storage.local.get(function(items) {
    _authenticated = items.at !== null && items.ats != null;
    callback(_authenticated);
  });
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if(request.message) {
    switch(request.message) {
      case 'isAuthenticated':
        isAuthenticated(sendResponse);
        break;
      case 'pollMessages':
        if(_polling) {
          return
        }
        getMessages();
        setTimeout(getMessages, 90000);
        _polling = true;
        break;
      case 'scrapeAuthParams':
        scrapeAuthParams(request.data);
    }
  }
});